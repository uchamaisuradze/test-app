import React from 'react';
import { Grid } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import TestPage from '../page/TestPage/TestPage';

const useStyles = makeStyles(theme => createStyles({
  root: {
    height: `calc(100vh - ${theme.layouts.header.height + theme.layouts.footer.height}px)`,
  },
}));

const Main = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.root}>
      <TestPage />
    </Grid>
  );
};

export default Main;
