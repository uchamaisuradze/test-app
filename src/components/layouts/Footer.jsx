import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Grid, Box, Button } from '@material-ui/core';
import {
  ArrowBack as ArrowBackIcon,
  ArrowForward as ArrowForwardIcon,
} from '@material-ui/icons';
import ButtonStep from '../common/ButtonStep';

const dumpData = [
  [
    { id: '1', correct: true, flag: true },
    { id: '2', correct: false, flag: false },
    { id: '3', correct: false, flag: true },
    { id: '4', correct: true, flag: true },
    { id: '5', correct: false, flag: false, current: true },
  ],
  [
    { id: '6', correct: true, flag: true },
    { id: '7', correct: false, flag: true },
    { id: '8', correct: false, flag: true },
    { id: '9', correct: false, flag: false },
  ],
  [
    { id: '10', correct: true, flag: true },
    { id: '11', correct: true, flag: true },
    { id: '12', correct: true, flag: true },
    { id: '13', correct: true, flag: true },
    { id: '14', correct: false, flag: false },
  ],
  [
    { id: '15', correct: true, disabled: true },
    { id: '16', correct: true, disabled: true },
    { id: '17', correct: true, disabled: true },
    { id: '18', correct: false, disabled: true },
    { id: '19', correct: true, disabled: true },
    { id: '20', correct: false, disabled: true },
    { id: '21', correct: false, disabled: true },
    { id: '22', correct: false, disabled: true },
  ],
];

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      height: theme.layouts.footer.height,
      padding: theme.spacing(0, 1.4),
      borderTop: `1px solid ${theme.border.color}`,
      backgroundColor: theme.palette.background.default,
      position: 'relative',
    },
    stepList: {
      maxWidth: 'calc(100vw - 150px)',
      overflowX: 'auto',
      overflowY: 'hidden',
      '& > *': {
        margin: theme.spacing(0, 0.375),
      },
    },
    navigation: {
      '& > *': {
        margin: theme.spacing(0, 0.5),
        padding: theme.spacing(0.725, 1.5),
        maxHeight: 34,
        fontSize: theme.typography.pxToRem(26),
      },
    },
    spacer: {
      width: 1,
      height: 25,
      marginTop: 1,
      marginLeft: 8,
      marginRight: 8,
      backgroundColor: '#d4d7e2',
    },
  }),
);

const Footer = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.root} justify="space-between" alignItems="center">
      <Box display="flex" className={classes.stepList}>
        {dumpData.map((dataArr, key) => (
          <React.Fragment key={uuidv4()}>
            {key > 0 && <Box className={classes.spacer} />}
            <ButtonStep color="purple">G</ButtonStep>
            {dataArr.map(data => (
              <ButtonStep
                key={uuidv4()}
                color={data.correct ? 'primary' : 'secondary'}
                disabled={data.disabled}
                flag={data.flag}
                active={data.current}
              >
                {data.id}
              </ButtonStep>
            ))}
          </React.Fragment>
        ))}
      </Box>
      <Box display="flex" className={classes.navigation}>
        <Button variant="contained">
          <ArrowBackIcon fontSize="inherit" />
        </Button>
        <Button variant="contained">
          <ArrowForwardIcon fontSize="inherit" />
        </Button>
      </Box>
    </Grid>
  );
};

export default Footer;
