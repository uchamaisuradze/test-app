import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Grid, Box, IconButton, Typography } from '@material-ui/core';
import {
  Close as CloseIcon,
  Info as InfoIcon,
  List as ListIcon,
  VisibilityOff as VisibilityOffIcon,
  DescriptionOutlined as DescriptionOutlinedIcon,
  MoreHoriz as MoreHorizIcon,
} from '@material-ui/icons';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      width: '100%',
      height: theme.layouts.header.height,
      padding: theme.spacing(0, 1.4),
      borderBottom: `1px solid ${theme.border.color}`,
      [theme.breakpoints.down('xs')]: {
        height: theme.layouts.xs.header.height,
      },
    },
    closeIconButton: {
      marginRight: theme.typography.pxToRem(5),
      padding: theme.spacing(1),
      [theme.breakpoints.down('xs')]: {
        display: 'none',
      },
    },
    closeIcon: {
      fontSize: theme.typography.pxToRem(30),
      color: theme.palette.primary.light,
    },
    infoIcon: {
      marginLeft: theme.typography.pxToRem(10),
      [theme.breakpoints.down('xs')]: {
        display: 'none',
      },
    },
    rightBox: {
      fontSize: theme.typography.pxToRem(28),
      '& > *': {
        margin: theme.spacing(0, 1),
      },
      '& > button': {
        padding: theme.spacing(1),
      },
    },
    spacer: {
      width: theme.typography.pxToRem(1),
      height: theme.typography.pxToRem(32),
      backgroundColor: theme.border.color,
    },
  }),
);

const Header = () => {
  const classes = useStyles();

  return (
    <Grid
      container
      alignItems="center"
      justify="space-between"
      className={classes.root}
    >
      <Box display="flex" alignItems="center">
        <IconButton className={classes.closeIconButton}>
          <CloseIcon className={classes.closeIcon} />
        </IconButton>
        <Typography variant="h2">Test 32</Typography>
        <InfoIcon color="disabled" fontSize="small" className={classes.infoIcon} />
      </Box>
      <Box display="flex" alignItems="center" className={classes.rightBox}>
        <IconButton disabled>
          <ListIcon color="disabled" />
        </IconButton>
        <IconButton disabled>
          <VisibilityOffIcon color="disabled" />
        </IconButton>
        <IconButton>
          <DescriptionOutlinedIcon />
        </IconButton>
        <Box className={classes.spacer} />
        <IconButton>
          <MoreHorizIcon />
        </IconButton>
      </Box>
    </Grid>
  );
};

export default Header;
