import React from 'react';
import { Grid, Box, Typography, List, ListItemText } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import {
  Edit as EditIcon,
  ArrowDropUp as ArrowDropUpIcon,
  ArrowDropDown as ArrowDropDownIcon,
  Bookmark as BookmarkIcon,
} from '@material-ui/icons';
import Rating from '../../../common/Rating';
import ResultListItem from '../../../common/ResultListItem';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      height: '100%',
    },
    gameDescriptionBox: {
      margin: theme.spacing(2, 0),
      '& > ul': {
        padding: theme.spacing(2, 6),
        '& > div': {
          margin: 0,
        },
      },
    },
    gameIcon: {
      marginLeft: theme.typography.pxToRem(8),
      color: theme.border.color,
    },
    resultBox: {
      marginTop: 'auto',
      width: '100%',
      backgroundColor: theme.palette.background.frame,
      borderRadius: theme.typography.pxToRem(4),
      marginBottom: 10,
    },
    resultBoxHeader: {
      height: 72,
      alignItems: 'center',
      padding: theme.spacing(0, 3),
      borderBottom: `1px solid ${theme.border.color}`,
    },
    resultBoxContent: {
      padding: theme.spacing(4, 3),
      '& .MuiTypography-caption': {
        color: '#50577b',
      },
    },
    arrowUpBox: {
      width: 16,
      height: 16,
      fontSize: 25,
    },
    arrowDownBox: {
      width: 16,
      height: 16,
      alignItems: 'flex-end',
      fontSize: 25,
    },
    editIcon: {
      fontSize: 15,
      margin: theme.spacing(0, 2),
    },
    bookmarkIcon: {
      fontSize: 15,
    },
    resultList: {
      marginTop: 21,
      '& > .MuiBox-root': {
        marginTop: 8,
      },
    },
  }),
);

const Description = () => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" className={classes.root}>
      <Box display="flex" alignItems="center">
        <Typography variant="h4">Game 1</Typography>
        <EditIcon fontSize="small" className={classes.gameIcon} />
      </Box>

      <Box className={classes.gameDescriptionBox}>
        <Typography>
          Seven singers—Jamie, Ken, Lalitha, Maya, Norton, Olive, and
          Patrick—will be scheduled to perform in the finals of a singing
          competition. During the evening of the competition, each singer,
          performing alone, will give exactly one performance. The schedule
          for the evening must conform to the following requirements:
        </Typography>
        <List>
          <ListItemText>Jamie performs immediately after Ken.</ListItemText>
          <ListItemText>
            Patrick performs at some time after Maya.
          </ListItemText>
          <ListItemText>
            LaliTexttha performs third only if Norton performs fifth.
          </ListItemText>
          <ListItemText>
            If Patrick does not perform second, he performs fifth.
          </ListItemText>
        </List>
      </Box>

      <Box
        display="flex"
        flexDirection="column"
        className={classes.resultBox}
      >
        <Grid container className={classes.resultBoxHeader}>
          <Grid item xs>
            <Typography variant="h3">Accuracy</Typography>
            <Typography variant="caption">Score</Typography>
          </Grid>
          <Grid item xs>
            <Typography variant="h3">14/19 (-5)</Typography>
            <Typography variant="caption">15/29 (-11)</Typography>
          </Grid>
          <Grid item xs>
            <Typography variant="h3">74% correct</Typography>
            <Typography variant="caption">57% correct</Typography>
          </Grid>
          <Grid item xs={1}>
            <Box display="flex" className={classes.arrowUpBox}>
              <ArrowDropUpIcon color="action" fontSize="inherit" />
            </Box>
            <Box display="flex" className={classes.arrowDownBox}>
              <ArrowDropDownIcon color="action" fontSize="inherit" />
            </Box>
          </Grid>
        </Grid>

        <Grid container className={classes.resultBoxContent}>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            width="100%"
          >
            <Box display="flex" flexDirection="column">
              <Typography variant="h3">Singing Competition</Typography>
              <Box display="flex" alignItems="center">
                <Rating defaultValue={5} name="rating" readOnly />
                <Typography variant="caption">
                  Single • Natural Sciences
                </Typography>
              </Box>
            </Box>
            <Box display="flex" alignItems="center">
              <Typography variant="caption">8 min</Typography>
              <EditIcon className={classes.editIcon} />
              <BookmarkIcon
                color="disabled"
                className={classes.bookmarkIcon}
              />
            </Box>
          </Box>
          <Box
            display="flex"
            width="100%"
            flexDirection="column"
            className={classes.resultList}
          >
            <ResultListItem
              index={1}
              color="primary"
              title="Parallel"
              rate={2}
              flag
            />
            <ResultListItem
              index={2}
              color="primary"
              title="Flaw"
              rate={3}
            />
            <ResultListItem
              index={3}
              color="secondary"
              title="Necessary Assumption"
              rate={3}
              edit
            />
            <ResultListItem
              index={4}
              color="secondary"
              title="Evaluate"
              rate={3}
            />
            <ResultListItem
              index={5}
              color="secondary"
              title="Sufficient Assumption"
              rate={3}
              flag
            />
          </Box>
        </Grid>
      </Box>
    </Box>
  );
};

export default Description;
