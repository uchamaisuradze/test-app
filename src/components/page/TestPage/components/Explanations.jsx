/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Box, AppBar, Tabs, Tab, Typography } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import {
  Assignment as AssignmentIcon,
  Edit as EditIcon,
  Headset as HeadsetIcon,
  ArrowUpward as ArrowUpwardIcon,
  ArrowDownward as ArrowDownwardIcon,
} from '@material-ui/icons';

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      height: '100%',
      '& .MuiAppBar-colorPrimary': {
        backgroundColor: 'transparent',
        color: '#2c3143',
        boxShadow: 'inset 0 1px 0 0 #aab2cf, inset 0 -1px 0 0 #aab2cf',
        '& .MuiTabs-flexContainer': {
          paddingLeft: 20,
        },
        '& .MuiTab-labelIcon': {
          minHeight: 60,
          minWidth: 120,
          '&.MuiTab-textColorInherit:not(.Mui-selected)': {
            opacity: 0.5,
          },
        },
        '& .MuiTabScrollButton-root': {
          width: 20,
        },
        '& .MuiTab-wrapper': {
          flexDirection: 'row',
          textTransform: 'none',
          fontSize: 16,
          fontWeight: 600,
          letterSpacing: '-0.1px',
          '& .MuiSvgIcon-root': {
            fontSize: 17,
            marginRight: 10,
            marginTop: 5,
          },
        },
        '& .MuiTabs-indicator': {
          backgroundColor: theme.palette.primary.main,
          height: 4,
        },
      },
    },
    tabPanel: {
      padding: theme.spacing(2, 3),
    },
    tabSection: {
      boxShadow: 'inset 0 -1px 0 0 #aab2cf',
      marginBottom: 15,
      paddingBottom: 10,
      '& .MuiSvgIcon-root': {
        fontSize: 16,
      },
      '& .MuiTypography-caption': {
        marginLeft: 10,
      },
    },
    sectionNumber: {
      color: '#50577b',
      fontSize: 14,
      '& > span': {
        marginRight: 15,
      },
    },
  }),
);

const Explanations = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box display="flex" flexDirection="column" className={classes.root}>
      <AppBar position="static">
        <Tabs scrollButtons="on" value={value} onChange={handleChange}>
          <Tab
            label="Explanations"
            icon={<AssignmentIcon />}
            {...a11yProps(0)}
          />
          <Tab label="Add note" icon={<EditIcon />} {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <Box display="flex" flexDirection="column" className={classes.tabPanel}>
        <Box>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
            className={classes.tabSection}
          >
            <Box display="flex" alignItems="center">
              <HeadsetIcon />
              <Typography variant="caption">Explanation</Typography>
            </Box>
            <Box className={classes.sectionNumber}>
              <span>800</span>
              <ArrowUpwardIcon />
              <ArrowDownwardIcon />
            </Box>
          </Box>
        </Box>
        <Box>
          <Typography>
            That first sentence is a head-scratcher. Why is it more important
            that we criticize democracies for their human rights violations than
            dictatorships that have done the same thing, but more violently? Why
            hold ourselves to a higher standard than murderous dictators? WTF?
            This seems like a conclusion, which makes me expect evidence from
            the remainder of the argument.
          </Typography>
          <Typography>
            That first sentence is a head-scratcher. Why is it more important
            that we criticize democracies for their human rights violations than
            dictatorships that have done the same thing, but more violently? Why
            hold ourselves to a higher standard than murderous dictators? WTF?
            This seems like a conclusion, which makes me expect evidence from
            the remainder of the argument.
          </Typography>
          <Typography>
            That first sentence is a head-scratcher. Why is it more important
            that we criticize democracies for their human rights violations than
            dictatorships that have done the same thing, but more violently? Why
            hold ourselves to a higher standard than murderous dictators? WTF?
            This seems like a conclusion, which makes me expect evidence from
            the remainder of the argument.
          </Typography>
          <Typography>
            That first sentence is a head-scratcher. Why is it more important
            that we criticize democracies for their human rights violations than
            dictatorships that have done the same thing, but more violently? Why
            hold ourselves to a higher standard than murderous dictators? WTF?
            This seems like a conclusion, which makes me expect evidence from
            the remainder of the argument.
          </Typography>
          <Typography>
            That first sentence is a head-scratcher. Why is it more important
            that we criticize democracies for their human rights violations than
            dictatorships that have done the same thing, but more violently? Why
            hold ourselves to a higher standard than murderous dictators? WTF?
            This seems like a conclusion, which makes me expect evidence from
            the remainder of the argument.
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default Explanations;
