import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Bookmark as BookmarkIcon } from '@material-ui/icons';
import Answer from '../../../common/Answer';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      height: '100%',
    },
    questionBox: {
      margin: theme.spacing(2, 0),
      '& > .MuiTypography-root': {
        marginBottom: 20,
      },
    },
    questionIcon: {
      marginLeft: theme.typography.pxToRem(12),
      color: '#50577b',
    },
  }),
);

const Question = () => {
  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="column" className={classes.root}>
      <Box display="flex" alignItems="center">
        <Typography variant="h4">Question 5</Typography>
        <BookmarkIcon fontSize="small" className={classes.questionIcon} />
      </Box>

      <Box className={classes.questionBox}>
        <Typography>
          Which one of the following is an acceptable schedule for the evenings
          performers, from first through seventh?
        </Typography>
        <Box display="flex" flexDirection="column">
          <Answer
            title="Ken, Jamie, Maya, Lalitha, Patrick, Norton, Olive"
            index="A"
          />
          <Answer
            title="Ken, Jamie, Maya, Lalitha, Patrick, Norton, Olive"
            index="B"
          />
          <Answer
            title="Ken, Jamie, Maya, Lalitha, Patrick, Norton, Olive"
            index="C"
          />
          <Answer
            title="Ken, Jamie, Maya, Lalitha, Patrick, Norton, Olive"
            index="D"
          />
        </Box>
      </Box>

    </Box>
  );
};

export default Question;
