import React from 'react';
import { Grid } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Description from './components/Description';
import Question from './components/Question';
import Explanations from './components/Explanations';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      maxHeight: '100%',
      position: 'relative',
      [theme.breakpoints.down('xs')]: {
        '&::after': {
          content: '""',
          width: '100%',
          height: '100%',
          top: 0,
          left: 0,
          backgroundColor: '#979797',
          opacity: 0.5,
          zIndex: 21,
        },
      },
      '& > .MuiGrid-item': {
        backgroundColor: '#ffffff',
        height: '100%',
        overflow: 'auto',
        padding: theme.spacing(2, 3),
        [theme.breakpoints.down('xs')]: {
          position: 'absolute',
          padding: theme.spacing(1.5, 1.5),
        },
      },
    },
    gameGrid: {
      zIndex: 20,
    },
    mainGrid: {
      zIndex: 30,
      [theme.breakpoints.down('xs')]: {
        width: '90%',
        left: '10%',
      }
    },
    explanationsGrid: {
      backgroundColor: '#f2f3f8 !important',
      padding: '0!important',
      zIndex: 10,
    },
  }),
);

const TestPage = () => {
  const classes = useStyles();
  return (
    <Grid container className={classes.root}>
      <Grid item xs className={classes.gameGrid}>
        <Description />
      </Grid>
      <Grid item sm={12} md={4} className={classes.mainGrid}>
        <Question />
      </Grid>
      <Grid item xs className={classes.explanationsGrid}>
        <Explanations />
      </Grid>
    </Grid>
  );
};

export default TestPage;
