import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Button } from '@material-ui/core';
import { Remove as RemoveIcon } from '@material-ui/icons';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      margin: theme.spacing(1, 0),
      '& .MuiRating-root': {
        marginRight: '0!important',
        '& .MuiButton-contained': {
          boxShadow: 'none',
        },
      },
      '& .MuiTypography-h4': {
        marginLeft: 16,
        color: '#000000',
        fontWeight: 'normal',
        textTransform: 'none',
        textAlign: 'left',
      },
      '&:hover $indexElement': {
        backgroundColor: '#2c3143',
        color: '#ffffff',
        '& $indexElementHover': {
          opacity: 1,
        },
      },
    },
    removeIcon: {
      fontSize: 15,
      margin: theme.spacing(0, 2),
    },
    indexElement: {
      width: 32,
      height: 24,
      borderRadius: 4,
      backgroundColor: '#eaecf3',
      transition: '0.3s all',
      position: 'relative',
      flexShrink: '0',
    },
    indexElementHover: {
      position: 'absolute',
      width: 28,
      height: 20,
      borderRadius: 3,
      top: 2,
      left: 2,
      border: '2px solid #ffffff',
      opacity: 0,
      transition: '0.3s all',
    },
  }),
);

const Answer = ({ index, title }) => {
  const classes = useStyles();

  return (
    <Button className={classes.root}>
      <Box display="flex" alignItems="center">
        <Box className={classes.indexElement}>
          {index}
          <Box className={classes.indexElementHover} />
        </Box>
        <Typography variant="h4">{title}</Typography>
      </Box>
      <Box display="flex" alignItems="center">
        <RemoveIcon color="disabled" className={classes.removeIcon} />
      </Box>
    </Button>
  );
};

export default Answer;
