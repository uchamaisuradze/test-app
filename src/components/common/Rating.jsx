import React from 'react';
import { Rating as MuiRating } from '@material-ui/lab';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      '&.MuiRating-iconEmpty .MuiBox-root': {
        backgroundColor: '#aab2cf',
      },
    },
    rateComponent: {
      width: 9,
      height: 8,
      backgroundColor: '#2c3143',
      margin: theme.spacing(0, 0.125, 0, 0),
    },
    start: {
      borderTopLeftRadius: 100,
      borderBottomLeftRadius: 100,
    },
    end: {
      borderTopRightRadius: 100,
      borderBottomRightRadius: 100,
    },
  }),
);

const RateComponent = ({ type }) => {
  const classes = useStyles();

  return <Box className={`${classes.rateComponent} ${classes[type] || ''}`} />;
};

const customIcons = {
  1: {
    icon: <RateComponent type="start" />,
    label: 'Very Dissatisfied',
  },
  2: {
    icon: <RateComponent />,
    label: 'Dissatisfied',
  },
  3: {
    icon: <RateComponent />,
    label: 'Neutral',
  },
  4: {
    icon: <RateComponent />,
    label: 'Satisfied',
  },
  5: {
    icon: <RateComponent type="end" />,
    label: 'Very Satisfied',
  },
};

const IconContainer = props => {
  const classes = useStyles();
  const { value, ...other } = props;
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <span {...other} className={`${other.className} ${classes.root}`}>
      {customIcons[value].icon}
    </span>
  );
};

const Rating = props => (
  <MuiRating
    style={{
      height: 8,
      marginRight: 11,
    }}
    IconContainerComponent={IconContainer}
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...props}
  />
);

export default Rating;
