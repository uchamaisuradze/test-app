import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Box, Button } from '@material-ui/core';
import { Bookmark as BookmarkIcon } from '@material-ui/icons';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      position: 'relative',
      '& > button': {
        padding: theme.spacing(0.125, 1),
      },
    },
    purple: {
      backgroundColor: '#765dbe',
      color: '#ffffff',
      '&:hover': {
        backgroundColor: '#765dbe',
      },
    },
    flagIcon: {
      color: '#d4d7e2',
      position: 'absolute',
      top: theme.typography.pxToRem(-18),
      left: '50%',
      fontSize: theme.typography.pxToRem(16),
      transform: 'translateX(-50%)',
    },
    active: {
      width: '70%',
      height: 4,
      borderRadius: theme.typography.pxToRem(2),
      background: theme.border.color,
      position: 'absolute',
      left: '15%',
      bottom: theme.typography.pxToRem(-10),
    },
  }),
);

const ButtonStep = ({
  color,
  children,
  flag = false,
  active = false,
  disabled,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {flag && (
        <BookmarkIcon className={classes.flagIcon} />
      )}
      <Button
        color={['primary', 'secondary', 'default'].includes(color) ? color : undefined}
        className={classes[color] || ''}
        variant="contained"
        disabled={disabled}
      >
        {children}
      </Button>
      {active && (
        <Box className={classes.active} />
      )}
    </Box>
  );
};

export default ButtonStep;
