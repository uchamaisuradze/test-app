import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import {Box, Typography} from '@material-ui/core';
import {
  Bookmark as BookmarkIcon,
  Edit as EditIcon,
} from '@material-ui/icons';
import ButtonStep from './ButtonStep';
import Rating from './Rating';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& .MuiRating-root': {
        marginRight: '0!important',
      },
      '& .MuiTypography-caption': {
        marginLeft: 16,
      },
    },
    editIcon: {
      fontSize: 15,
      margin: theme.spacing(0, 2),
    },
    bookmarkIcon: {
      fontSize: 15,
    },
  }),
);

const ResultListItem = ({ color, index, title, rate, edit, flag }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box display="flex" alignItems="center">
        <ButtonStep color={color}>{index}</ButtonStep>
        <Typography variant="caption">{title}</Typography>
      </Box>
      <Box display="flex" alignItems="center">
        <Rating defaultValue={rate} readOnly />
        <EditIcon color={edit ? 'primary' : 'disabled'} className={classes.editIcon} />
        <BookmarkIcon color={flag ? 'primary' : 'disabled'} className={classes.bookmarkIcon} />
      </Box>
    </Box>
  );
};

export default ResultListItem;
