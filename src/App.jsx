import React from 'react';
import 'fontsource-roboto';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Header from './components/layouts/Header';
import Main from './components/layouts/Main';
import Footer from './components/layouts/Footer';

const theme = createMuiTheme({
  palette: {
    background: {
      default: '#FFFFFF',
      frame: '#f2f3f8',
    },
    primary: {
      dark: '#000000',
      main: '#2c3143',
      light: '#818bb2',
      grey: '#aab2cf',
    },
    secondary: {
      main: '#c2423a',
    },
  },
  overrides: {
    MuiSvgIcon: {
      root: {
        fontSize: 28,
        color: '#2c3143',
        borderRadius: 4,
      },
      fontSizeSmall: {
        fontSize: 18,
      },
      colorAction: {
        color: '#818bb2',
      },
      colorDisabled: {
        color: '#aab2cf',
      },
    },
    MuiButton: {
      root: {
        minWidth: 34,
        fontSize: 14,
        fontWeight: 'bold',
      },
      contained: {
        backgroundColor: '#eaecf3',
      },
      containedPrimary: {
        backgroundColor: '#44464e',
        '&.Mui-disabled': {
          backgroundColor: '#aab2cf',
          color: '#d2d8ed',
        },
      },
      containedSecondary: {
        '&.Mui-disabled': {
          backgroundColor: '#e07b75',
          color: '#f3b3b3',
        },
      },
    },
  },
  border: {
    color: '#d4d7e2',
  },
  typography: {
    body1: {
      color: '#2c3143',
      fontSize: 16,
      fontFamily: 'Roboto',
      lineHeight: 'normal',
    },
    regular: {
      color: '#000000',
    },
    h2: {
      fontSize: 24,
      fontWeight: 'bold',
      letterSpacing: '-1px',
      color: '#2c3143',
      padding: 0,
      margin: 0,
    },
    h3: {
      fontSize: 18,
      fontWeight: 600,
      letterSpacing: '-0.45px',
      color: '#2c3143',
      marginBottom: 3,
    },
    h4: {
      fontSize: 16,
      fontWeight: 600,
      color: '#818bb2',
      letterSpacing: '-0.2px',
    },
    caption: {
      fontSize: 14,
      fontWeight: 'normal',
      letterSpacing: '-0.33px',
      color: '#818bb2',
    },
  },
  layouts: {
    header: {
      height: 72,
    },
    footer: {
      height: 80,
    },
    xs: {
      header: {
        height: 60,
      },
      footer: {
        height: 72,
      },
    },
  },
});

const App = () => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Header />
    <Main />
    <Footer />
  </MuiThemeProvider>
);

export default App;
