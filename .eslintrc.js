module.exports = {
  env: {
    browser: true,
    es2020: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'arrow-parens': 'off',
    'react/prop-types': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'implicit-arrow-linebreak': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'import/prefer-default-export': 'off',
    'operator-linebreak': 'off',
    'object-curly-newline': 'off',
    'no-confusing-arrow': 'off',
    'react/button-has-type': 'off',
    'react/jsx-curly-newline': 'off',
    'function-paren-newline': 'off',
    'jsx-a11y/mouse-events-have-key-events': 'off',
    'no-empty': 'off',
    'no-alert': 'off',
  },
};
